import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.Part;
@ManagedBean
@ViewScoped



public class fileUpload {
 
 private Part uploadedFile;
 private String username;
 
 //The reason for the ../ file path changing is that we need to move out of the glassfish server file
 //This allows us to come back to the AuthLogin project and insert the images into the required folder

 File file = new File("../../../../../web/resources/images");
 private String folder = file.getAbsolutePath();
 

  
 
 
 public Part getUploadedFile() {
 return uploadedFile;
 }
 
 public void setUploadedFile(Part uploadedFile) {
 this.uploadedFile = uploadedFile;
 }
 
 
 public void saveFile(){
    String userHome = System.getProperty("user.dir");
    System.out.print("-------------->" + folder);

 try (InputStream input = uploadedFile.getInputStream()) {
 String fileName = uploadedFile.getSubmittedFileName();
        if(fileName.contains(".jpg") && ((uploadedFile.getSize() / 1048576) <= 10) ){
         Files.copy(input, new File(folder, fileName).toPath());
        }else{
            System.out.println("False");
        }
     }
     catch (IOException e) {
         e.printStackTrace();
     }
 }
 
}

